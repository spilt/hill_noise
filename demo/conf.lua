function love.conf(t)
    t.version = "0.10.2"
    t.window.title = "Noise Demo"
    t.window.width = 1024
    t.window.height = 768
    t.window.msaa = 2
    t.window.highdpi = true

    t.accelerometerjoystick = false
    t.externalstorage = false

    t.modules.audio = false
    t.modules.image = false
    t.modules.joystick = false
    t.modules.physics = false
    t.modules.sound = false
    t.modules.system = false
    t.modules.touch = false
    t.modules.video = false
    t.modules.thread = false
end
