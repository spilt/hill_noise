#define TAU 6.283185307179586476925286766559005768394338798750211641949
#define MAX_RESOLUTION 64
extern int resolution;
extern float sigma;
extern float amplitudes[MAX_RESOLUTION];
extern float offsets[MAX_RESOLUTION];
extern float range_min, range_max;
extern bool draw_outline = false;

float cdf(float x) {
    return .5 + .5*sign(x)*sqrt(1.-exp(-4./TAU * x*x));
}

float noise1d(float x) {
    float noise = 0.;
    for (int i=0; i < resolution; i++) {
        float a = amplitudes[i];
        noise += a*cos(x/a + offsets[i]);
    }
    return cdf(noise/sigma);
}

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
{
    float x = mix(range_min,range_max,texture_coords.x);
    float n = noise1d(x);
    if (draw_outline) {
        return texture_coords.y > (1.-n) ? vec4(1.,1.,1.,1.) : vec4(0.,0.,0.,1.);
    } else {
        return vec4(n,n,n,1.);
    }
}
