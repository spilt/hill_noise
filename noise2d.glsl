#define TAU 6.283185307179586476925286766559005768394338798750211641949
#define PHI 1.618033988749894848204586834365638117720309179805762862135
#define MAX_RESOLUTION 64
extern int resolution;
extern float sigma;
extern float amplitudes[MAX_RESOLUTION];
extern vec2 offsets[MAX_RESOLUTION];
extern vec2 range_min, range_max;

float cdf(float x) {
    return .5 + .5*sign(x)*sqrt(1.-exp(-4./TAU * x*x));
}

float noise2d(vec2 pos) {
    float noise = 0.;
    for (int i=0; i < resolution; i++) {
        float angle = mod(float(i)*PHI, 1.)*TAU;
        float u = pos.x*cos(angle) - pos.y*sin(angle);
        float v = pos.x*cos(angle+TAU/4.) - pos.y*sin(angle+TAU/4.);
        float a = amplitudes[i];
        noise += a*mix(cos(u/a + offsets[i].x), cos(v/a + offsets[i].y), .5);
    }
    return cdf(noise/sigma);
}


vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
{
    vec2 coords = mix(range_min,range_max,texture_coords);
    float n = noise2d(coords);
    return vec4(n,n,n,1.);
}
